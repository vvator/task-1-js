console.log('>>>Array Tasks 2<<<');
console.log('>>Finding max, min, avg for square arrays<<\n');

function createRandomMatrix() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 15);
    for (var i = 0; i < len; i++) {
        var row = [];
        for (var j = 0; j < len; j++) {
            row[j] = getRandomIntInRange(100, 1000);
        }
        arr[i] = row;
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function printArray(arr) {
    'use strict';
    for (var i = 0; i < arr.length; i++) {
        console.log(arr[i]);
    }
    console.log();
}

function perform(arr) {
    'use strict';
    var max = arr[0][0];
    var min = arr[0][0];
    var sum = 0;
    var count = 0;
    for (var i in arr) {
        for (var j in arr[i]) {
            if (max < arr[i][j]) {
                max = arr[i][j];
            }
            if (min > arr[i][j]) {
                min = arr[i][j];
            }

            sum += arr[i][j];
            count++;
        }
    }
    /*console.log("sum: " + sum);*/
    console.log("avg: " + (sum / count));
    console.log("max: " + max);
    console.log("min: " + min);
}

var arr = createRandomMatrix();
printArray(arr);
perform(arr);