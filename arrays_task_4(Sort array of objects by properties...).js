console.log('>>>Array Tasks 4<<<');
console.log('>>Sort array of objects by properties count, ascending and descending<<\n');

function objSort(arrObj, sort) {
    'use strict';
    var resAscend = [];
    var resDescend = [];
    var resResult = [];
    for (var i = 0; i < arrObj.length; i++) {
        var obj = arrObj[i];
        var keys = 0;
        for (var key in obj) {
            keys++;
        }
        resAscend[keys - 1] = obj;
        resDescend[arrObj.length - keys] = obj;
    }
    if (sort.toUpperCase() === 'desc'.toUpperCase()) {
        return resDescend;
    }
    return resAscend;
}

var obj1 = { a: 2, c: 3, d: 3 };
var obj2 = { a: 1 };
var obj3 = { a: 2, c: 3 };
var arOfObj = [obj1, obj2, obj3];

console.log('ASC sort:');
console.log(objSort(arOfObj, 'asc'));
console.log('DESC sort:');
console.log(objSort(arOfObj, 'desc'));
