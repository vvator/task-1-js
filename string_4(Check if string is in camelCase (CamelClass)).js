console.log('>>>String Tasks 1<<<');
console.log('>>Check if string is in camelCase (CamelClass) - boolean<<\n');

function isCamelCase(str) {
    'use strict';
    if (str.match(new RegExp('^[a-z]+$'))) {
        console.log(false + '  -  ' + str);
        return false;
    }
    if (str.match(new RegExp('^[A-Z]+$'))) {
        console.log(false + '  -  ' + str);
        return false;
    }
    if (str.match(new RegExp('^[a-zA-Z]+$'))) {
        console.log(true + '  -  ' + str);
        return true;
    }
    console.log(false + '  -  ' + str);
    return false;
}

isCamelCase('VIKHLAEV');
isCamelCase('vikhlaev');
isCamelCase('VikhLAev');
isCamelCase('vIKHLlaev');
isCamelCase('vikhlaeV');
isCamelCase('vikHlaev');
isCamelCase('vik_Hlaev');
isCamelCase('Vihklaev');
isCamelCase('_vIkHlAeV_');
isCamelCase('VIKHLAEV_');
isCamelCase('_VIKHLAEV');
isCamelCase('VI_KH_LAE_V');
isCamelCase('v_ikh_lae_v');
