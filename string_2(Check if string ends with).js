console.log('>>>String Tasks 1<<<');
console.log('>>Check if string ends with - boolean<<\n');

function isEndsWith(str, subStr) {
    'use strict';
    var REGEX_PATTERN = '^.*' + subStr + '$';
    if (str.search(REGEX_PATTERN) > -1) {
        return true;
    }
    return false;
}

var str = 'vikhlaev';
var subStr = 'laev';
console.log();
console.log('string "' + str + '" ends with "' + subStr + '":  ' + isEndsWith(str, subStr));
subStr = 'vi';
console.log('string "' + str + '" ends with "' + subStr + '":  ' + isEndsWith(str, subStr));
