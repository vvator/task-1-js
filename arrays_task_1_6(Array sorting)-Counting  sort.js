console.log('>>>Array Tasks 1<<<');
console.log('>>CountingSort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function countingSort(arr) {
    'use strict';
    var n = arr.length;
    var count = [];
    var sorterArray = [];
    for (var k = 0; k < n; k++) {
        count[k] = 0;
    }
    for (var i = 0; i < n - 1; i++) {
        for (var j = i + 1; j < n; j++) {
            if (arr[i] < arr[j]) {
                count[j]++;
            } else {
                count[i]++;
            }
        }
    }
    for (var m = 0; m < n; m++) {
        sorterArray[count[m]] = arr[m];
    }
    return sorterArray;
}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
countingSort(arr);
console.log('after sort:');
console.log(arr);
