console.log('>>>Array Tasks 3<<<');
console.log('>>Printing some triangles with zeroes and ones<<\n');

function fillMatrix1(razmernost) {
    'use strict';
    for (var i = 0; i < razmernost; i++) {
        var row = [];
        for (var j = 0; j < razmernost; j++) {
            if (
                ((i > j || (razmernost - i - 1) > j) &&
                    (i < j || (razmernost - i - 1) < j)) ||
                (i === j || (razmernost - i - 1) === j)
            ) {
                row[j] = 1;
            } else {
                row[j] = 0;
            }
        }
        matrix1[i] = row;
    }
}

function fillMatrix2(razmernost) {
    'use strict';
    for (var i = 0; i < razmernost; i++) {
        var row = [];
        for (var j = 0; j < razmernost; j++) {

            if (i < j || (razmernost - i - 1) < j

            ) {
                row[j] = 0;
            } else {
                row[j] = 1;
            }
        }
        matrix2[i] = row;
    }
}

function printMatrix(array2D) {
    'use strict';
    for (var i in array2D) {
        var row = '';
        for (var j in array2D) {
            row += array2D[i][j] + ' ';
        }
        console.log(row);
    }
}

var matrix1 = [];
var matrix2 = [];

fillMatrix1(5);
fillMatrix2(5);

printMatrix(matrix1);
console.log('');
printMatrix(matrix2);
