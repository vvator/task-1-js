console.log('>>>Array Tasks 1<<<');
console.log('>>BubbleSort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function bubbleSort(arr) {
    'use strict';
    for (var j = 0; j < arr.length - 1; j++) {
        var isSwapped = false;
        var i = 0;
        while (i < (arr.length - 1 - j)) {
            if (arr[i] > arr[i + 1]) {
                var buffer = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = buffer;
                isSwapped = true;
            }
            i++;
        }
        if (!isSwapped) {
            break;
        }
    }
}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
bubbleSort(arr);
console.log('after sort:');
console.log(arr);
