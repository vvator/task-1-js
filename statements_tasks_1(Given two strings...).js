console.log('>>>Statements Tasks 1<<<');
console.log('>> simple and advanced part<<\n');

function parseSimple(str) {
    'use strict';
    var numb = str.split(/[g-z]+/).join('');
    var REGEXP_PATTERN = /[a-f]+/gi;
    if (numb.search(REGEXP_PATTERN) > -1) {
        numb = '0x' + numb.toUpperCase();
    }
    return numb;
}

function parseAdvancedWithSum(str) {
    'use strict';
    console.log('parse string: ' + str);
    var REGEXP_PATTERN = /[a-f]+/gi;
    var arrNumb = str.split(/[g-z]+/gi);
    var sum = 0;
    for (var i in arrNumb) {
        if (arrNumb[i].search(REGEXP_PATTERN) > -1) {
            arrNumb[i] = '0x' + arrNumb[i].toUpperCase();
            console.log('\thex: ' + arrNumb[i] + '   ->   dec: ' + Number(arrNumb[i]));
        } else {
            console.log('\tdec: ' + arrNumb[i]);
        }
        sum += Number(arrNumb[i]);
    }
    return sum;
}

var startStr1 = '123x1z13';
var startStr2 = 'a123';
console.log('Simple part: ');
console.log('string1: ' + startStr1);
console.log('string2: ' + startStr2);
console.log('result:');
console.log(parseSimple(startStr1) + ' + ' + parseSimple(startStr2) + ' = ' + (Number(parseSimple(startStr1)) + Number(parseSimple(startStr2))));


console.log();


startStr1 = '12yyy3x1z13ppdftd54ttdiidhdaajgtre4v1c';
startStr2 = '1cttttFf';
console.log('* Advanced part: ');
console.log('string1: ' + startStr1);
console.log('string2: ' + startStr2);
var sum1 = parseAdvancedWithSum(startStr1);
var sum2 = parseAdvancedWithSum(startStr2);
console.log('sum digits parsed from string1: ' + sum1);
console.log('sum digits parsed from string2: ' + sum2);
console.log('result:');
console.log(sum1 + ' + ' + sum2 + ' = ' + (sum1 + sum2));
