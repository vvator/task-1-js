console.log('>>>Array Tasks 1<<<');
console.log('>>QuickSort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function swap(arr, firstIndex, secondIndex) {
    'use strict';
    var temp = arr[firstIndex];
    arr[firstIndex] = arr[secondIndex];
    arr[secondIndex] = temp;
}

function partition(arr, left, right) {
    'use strict';
    var pivot = arr[Math.floor((right + left) / 2)];
    var i = left;
    var j = right;
    while (i <= j) {
        while (arr[i] < pivot) {
            i++;
        }
        while (arr[j] > pivot) {
            j--;
        }
        if (i <= j) {
            swap(arr, i, j);
            i++;
            j--;
        }
    }
    return i;
}

function quickSort(arr, left, right) {
    'use strict';
    if (arr.length > 1) {
        left = typeof left !== 'number' ? 0 : left;
        right = typeof right !== 'number' ? (arr.length - 1) : right;

        var index = partition(arr, left, right);

        if (left < index - 1) {
            quickSort(arr, left, index - 1);
        }
        if (index < right) {
            quickSort(arr, index, right);
        }
    }
}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
quickSort(arr);
console.log('after sort:');
console.log(arr);
