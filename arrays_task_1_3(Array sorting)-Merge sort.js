console.log('>>>Array Tasks 1<<<');
console.log('>>Merge sort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function mergeSort(arr) {
    'use strict';
    merge(arr, 0, arr.length - 1);
    return arr;
}

function merge(arr, low, high) {
    'use strict';
    if (low < high) {
        var mid = Math.floor((low + high) / 2);
        merge(arr, low, mid);
        merge(arr, mid + 1, high);
        var b = new Array(high + 1 - low);
        var j = mid + 1;
        var h = low;
        var i = 0;
        while (h <= mid && j <= high) {
            if (arr[h] <= arr[j]) {
                b[i] = arr[h];
                h++;
            } else {
                b[i] = arr[j];
                j++;
            }
            i++;
        }
        if (h > mid) {
            for (var k = j; k <= high; k++) {
                b[i] = arr[k];
                i++;
            }
        } else {
            for (var k = h; k <= mid; k++) {
                b[i] = arr[k];
                i++;
            }
        }
        for (var k = 0; k <= high - low; k++) {
            arr[k + low] = b[k];
        }
    }
}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
console.log('after sort:');
console.log(mergeSort(arr));
