console.log('>>>Statements Tasks 2<<<');
console.log('>> simple and advanced part<<\n');

var DELIMITER_PROPERTIES = ';';
var EMPTY_PROPERTIES = '';
var DELIMITER_KEY_VALUE = ',';
var ARRAY_KEY_DELIMITER = ':';

function parse(data) {
    'use strict';
    var outputObj = {};
    var properties = data.split(DELIMITER_PROPERTIES);
    for (var i in properties) {
        if (properties[i] !== EMPTY_PROPERTIES) {
            var properti = properties[i].split(DELIMITER_KEY_VALUE);
            var key = properti[0];
            var value = properti[1];
            outputObj[key] = value;
        }
    }
    return outputObj;
}

function parseAdvanced(data) {
    'use strict';
    var match = data.match(/(.*[;])(.*[:].*)/);
    var propString = match[1];
    var arrString = match[2];
    console.log(match[1]);
    console.log(match[2]);
    var outputObj = parse(match[1]);
    var properti = arrString.split(ARRAY_KEY_DELIMITER);
    var arrName = properti[0];
    var arrStrVals = properti[1];
    var arrValues = [];
    var elements = arrStrVals.split(DELIMITER_PROPERTIES);
    for (var i in elements) {
        if (elements[i] !== EMPTY_PROPERTIES) {
            var element = elements[i].split(DELIMITER_KEY_VALUE);
            var key = element[0];
            var value = element[1];
            var ob = {};
            ob[key] = value;
            arrValues[i] = ob;
        }
    }
    outputObj[arrName] = arrValues;
    return outputObj;
}

console.log('Simple part:');
var data = ";key,value;key1,value;key3,value3;";
var output = parse(data);
console.log('string with  data: ' + data);
console.log('output = ');
console.log(output);


console.log();


console.log('* Advanced part excersise:');
data = ";key,value;key1,value;arrayHere:k1,v1;k2,v2;k3,v3";
output = parseAdvanced(data);
console.log('string with  data: ' + data);
console.log('output = ');
console.log(output);
