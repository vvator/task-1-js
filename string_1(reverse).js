console.log('>>>String Tasks 1<<<');
console.log('>>Reverse String - string<<\n');

function reverse(startString) {
    'use strict';
    return startString.split("").reverse().join("");
}

var str = 'vikhlaev';
console.log('start string: ' + str);
console.log('reverse string: ' + reverse(str));
