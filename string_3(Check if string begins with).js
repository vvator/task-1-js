console.log('>>>String Tasks 1<<<');
console.log('>>Check if string begins with - boolean<<\n');

function isBeginsWith(str, subStr) {
    'use strict';
    var REGEX_PATTERN = '^' + subStr + '.*$';
    if (str.search(REGEX_PATTERN) > -1) {
        return true;
    }
    return false;
}

var str = 'vikhlaev';
var subStr = 'vikh';
console.log();
console.log('string "' + str + '" begins with "' + subStr + '":  ' + isBeginsWith(str, subStr));
subStr = 'klhaev';
console.log('string "' + str + '" begins with "' + subStr + '":  ' + isBeginsWith(str, subStr));
