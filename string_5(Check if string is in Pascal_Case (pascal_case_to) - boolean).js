console.log('>>>String Tasks 1<<<');
console.log('>>Check if string is in Pascal_Case (pascal_case_to) - boolean - boolean<<\n');

function isCamelCase(str) {
    'use strict';
    if (str.match(new RegExp('^[a-z]+[a-z,_]*[_]*[a-z,_]*[a-z]+$'))) {
        console.log(true + '  -  ' + str);
        return true;
    }

    console.log(false + '  -  ' + str);
    return false;
}

isCamelCase('VIKHLAEV');
isCamelCase('v_ik_hl_aev');
isCamelCase('VikhLAev');
isCamelCase('vIKHLlaev');
isCamelCase('vikhlaeV');
isCamelCase('vikHlaev');
isCamelCase('vik_Hlaev');
isCamelCase('Vihklaev');
isCamelCase('_vIkHlAeV_');
isCamelCase('VIKHLAEV_');
isCamelCase('_VIKHLAEV');
isCamelCase('VI_KH_LAE_V');
isCamelCase('v_ikh_lae_v');
