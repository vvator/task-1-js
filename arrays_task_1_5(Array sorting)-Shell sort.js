console.log('>>>Array Tasks 1<<<');
console.log('>>Shell sort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function shellSort(arr) {
    'use strict';
    var step = arr.length / 2;
    while (step > 0) {
        for (var i = 0; i < (arr.length - step); i++) {
            var j = i;
            while (j >= 0 && arr[j] > arr[j + step]) {
                var temp = arr[j];
                arr[j] = arr[j + step];
                arr[j + step] = temp;
                j--;
            }
        }
        step = step / 2;
    }

}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
shellSort(arr);
console.log('after sort:');
console.log(arr);