console.log('>>>Array Tasks 1<<<');
console.log('>>Insertion sort<<\n');

function createRandomArray() {
    'use strict';
    var arr = [];
    var len = getRandomIntInRange(5, 20);
    for (var i = 0; i < len; i++) {
        arr[i] = getRandomIntInRange(-5, 15);
    }
    console.log('Created random array with ' + len + ' elements');
    return arr;
}

function getRandomIntInRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function insertionSort(arr) {
    'use strict';
    for (var i = 1; i < arr.length; i++) {
        var currentElement = arr[i];
        var j = i;
        while (j > 0 && currentElement < arr[j - 1]) {
            arr[j] = arr[j - 1];
            j--;
        }
        arr[j] = currentElement;
    }
}

var arr = createRandomArray();
console.log('without sort:');
console.log(arr);
insertionSort(arr);
console.log('after sort:');
console.log(arr);
